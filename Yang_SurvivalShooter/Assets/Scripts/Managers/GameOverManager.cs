﻿using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;


    Animator anim;
    GameObject restartButton;


    void Awake()
    {
        anim = GetComponent<Animator>();
        restartButton = GameObject.FindGameObjectWithTag("RestartButton");

    }

    void Start(){
        restartButton.SetActive(false);
    }


    void Update()
    {
        if (playerHealth.heartCount <= 0)
        {
            anim.SetTrigger("GameOver");
            restartButton.SetActive(true);
        }
    }
}
