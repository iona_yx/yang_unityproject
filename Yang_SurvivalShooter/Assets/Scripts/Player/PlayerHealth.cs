﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider[] healthSliders;
    public GameObject[] heartImages;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);


    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    bool isDead;
    bool damaged;
    [HideInInspector]
    public int heartCount = 3;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        playerAudio = GetComponent <AudioSource> ();
        playerMovement = GetComponent <PlayerMovement> ();
        playerShooting = GetComponentInChildren <PlayerShooting> ();
        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }


    public void TakeDamage (int amount)
    {
        damaged = true;

        currentHealth -= amount;

        if (heartCount ==3 ){
            healthSliders[0].value = currentHealth;
        }else if(heartCount==2){
            healthSliders[1].value = currentHealth;

        }else if(heartCount==1){
            healthSliders[2].value = currentHealth;
        }

        if(currentHealth<=0){
            heartCount--;
            currentHealth = startingHealth;
            switch(heartCount){
                case 2:
                    Destroy(heartImages[0]);
                    Destroy(healthSliders[0].gameObject);
                    break;
                case 1:
                    Destroy(heartImages[1]);
                    Destroy(healthSliders[1].gameObject);
                    break;
                case 0:
                    Destroy(heartImages[2]);
                    Destroy(healthSliders[2].gameObject);
                    break;
            }
        }

        playerAudio.Play ();

        if(heartCount <=0)
        {
            print(heartCount);
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }
}
