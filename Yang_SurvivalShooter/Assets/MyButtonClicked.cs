﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MyButtonClicked : MonoBehaviour {

    Button btn;

    void Awake()
    {
        btn = GetComponent<Button>();
    }

    void Start()
    {
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(RestartButtonClicked);
    }

    void RestartButtonClicked(){
        SceneManager.LoadScene(0);
    }

}
