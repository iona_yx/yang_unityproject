﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigibody;
    int floorMask;
    float camRayLength = 100f;
    public float jumpSpeed = 50; 

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigibody = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        Jump();
        Move(h, v);
        Turning();
        Animating(h, v);
    }

    void Move (float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;
        playerRigibody.MovePosition(transform.position + movement);
    }

    void Jump(){
        if(Mathf.Abs(playerRigibody.velocity.y)<0.1f) {
            if(Input.GetKeyDown(KeyCode.Space)){
                playerRigibody.AddForce(Vector3.up * jumpSpeed);
            }

        }
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 PlayerToMouse = floorHit.point - transform.position;
            PlayerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(PlayerToMouse);
            playerRigibody.MoveRotation(newRotation);

        }
    }
    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }
}
